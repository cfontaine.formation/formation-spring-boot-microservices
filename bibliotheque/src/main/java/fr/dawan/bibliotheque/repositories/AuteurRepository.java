package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.bibliotheque.entities.Auteur;

@Repository
public interface AuteurRepository extends JpaRepository<Auteur, Long> {
    
   // @Query(value="FROM Auteur a WHERE a.deces is null")
    @Query(value="SELECT * FROM auteurs WHERE deces IS NULL",nativeQuery = true)
    List<Auteur> findAlive();
    
    List<Auteur> findByNom(String nom);
    
    @Query(value="FROM Auteur a JOIN a.livres l WHERE l.id=:idLivre")
    List<Auteur> findByLivreId(@Param("idLivre")long id);
    
    @Query(value="FROM Auteur a JOIN a.livres l GROUP BY a ORDER BY count(l) desc")
    List<Auteur> findTop5CountLivre(Pageable page);
    
}
