package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.bibliotheque.entities.Categorie;

@Repository
public interface CategoriRepository extends JpaRepository<Categorie, Long> {
    List<Categorie>findByNom(String nom);
}
