package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.bibliotheque.entities.Livre;

@Repository
public interface LivreRepositrory extends JpaRepository<Livre, Long>{

    public List<Livre> findByTitreLikeIgnoreCase(String titre);
    
//    @Query(value="FROM Livre l WHERE l.titre LIKE %:titre%")
//    List<Livre> searchLivreByTitre(@Param("titre") String titre);
    
    List<Livre> findByAnneeSortie(int anneSortie);
    
    List<Livre> findByAnneeSortieBetween(int min,int max);
}
