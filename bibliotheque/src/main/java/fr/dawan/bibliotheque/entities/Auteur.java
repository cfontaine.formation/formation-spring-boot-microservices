package fr.dawan.bibliotheque.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="auteurs")
public class Auteur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50, nullable = false)
    private String prenom;

    @Column(length = 50, nullable = false)
    private String nom;
    // @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private LocalDate naissance;

    private LocalDate deces;

    @ManyToOne
    private Nation nation;

    @ManyToMany(mappedBy = "auteurs")
    private List<Livre> livres = new ArrayList<>();

    @Version
    private int version;

    public Auteur() {
        super();
    }

    public Auteur(String prenom, String nom, LocalDate naissance) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
    }

    public Auteur(String prenom, String nom, LocalDate naissance, LocalDate deces) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
        this.deces = deces;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getNaissance() {
        return naissance;
    }

    public void setNaissance(LocalDate naissance) {
        this.naissance = naissance;
    }

    public LocalDate getDeces() {
        return deces;
    }

    public void setDeces(LocalDate deces) {
        this.deces = deces;
    }

    public List<Livre> getLivres() {
        return livres;
    }

    public void setLivres(List<Livre> livres) {
        this.livres = livres;
    }

    
    
    public Nation getNation() {
        return nation;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Auteur [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", naissance=" + naissance + ", deces="
                + deces + ", version=" + version + "]";
    }

}
