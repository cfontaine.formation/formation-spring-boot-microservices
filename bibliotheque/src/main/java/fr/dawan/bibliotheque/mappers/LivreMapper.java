package fr.dawan.bibliotheque.mappers;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import fr.dawan.bibliotheque.dto.LivreDto;
import fr.dawan.bibliotheque.entities.Livre;

@Component
public class LivreMapper {

    
    private ModelMapper mapper=new ModelMapper();
    
    
    public LivreDto toDto(Livre c) {
        mapper.typeMap(Livre.class, LivreDto.class).addMappings(mapper -> {
            mapper.map(src -> src.getId(), LivreDto::setId);
            mapper.map(src -> src.getTitre(), LivreDto::setTitre);
            mapper.map(src -> src.getAnneeSortie(), LivreDto::setAnneeSortie);
            mapper.map(src -> src.getCategorie().getNom(), LivreDto::setCategorie);

        });
        return mapper.map(c, LivreDto.class);
    }

    public Livre fromDto(LivreDto cDto) {
        mapper.typeMap(LivreDto.class, Livre.class).addMappings(mapper -> {
            mapper.map(src -> src.getId(), Livre::setId);
            mapper.map(src -> src.getAnneeSortie(), Livre::setAnneeSortie);
            mapper.map(src -> src.getTitre(), Livre::setTitre);
            mapper.<String>map(src -> src.getCategorie(), (dest, v) -> dest.getCategorie().setNom(v));
        });
        return mapper.map(cDto, Livre.class);
    }
}