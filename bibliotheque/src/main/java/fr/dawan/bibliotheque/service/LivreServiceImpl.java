package fr.dawan.bibliotheque.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import fr.dawan.bibliotheque.dto.LivreDto;
import fr.dawan.bibliotheque.entities.Livre;
import fr.dawan.bibliotheque.mappers.LivreMapper;
import fr.dawan.bibliotheque.repositories.LivreRepositrory;

@Service
public class LivreServiceImpl implements LivreService{
    
    @Autowired
    LivreRepositrory livreRepository;
    
    @Autowired 
    LivreMapper mapper;

    @Override
    public List<LivreDto> getAllLivre() {
       // ModelMapper m=new ModelMapper();
//        List<Livre> lstLivre=livreRepository.findAll();
//        List<LivreDto> lstLivreDto=new ArrayList<>();
//        for(Livre l : lstLivre)
//        {
//           lstLivreDto.add( m.map(l, LivreDto.class));
//        }
//        return lstLivreDto; 
        
//        ou      
        //l->m.map(l,LivreDto.class)
        return livreRepository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<LivreDto> getAllLivre(int page, int nbElts) {
        ModelMapper m=new ModelMapper();
        return livreRepository.findAll(PageRequest.of(page, nbElts)).getContent().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public LivreDto findById(long id) {
        Optional<Livre> livre=livreRepository.findById(id);
        if(livre.isPresent())
        {
           // ModelMapper m=new ModelMapper();
           // return m.map(livre.get(),LivreDto.class);
            return mapper.toDto(livre.get());
        }
        return null;
    }

    @Override
    public List<LivreDto> searchByTitre(String titre) {
        ModelMapper m=new ModelMapper();
        return livreRepository.findByTitreLikeIgnoreCase("%"+ titre + "%").stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void deleteById(long id) {
        livreRepository.deleteById(id);
    }

    @Override
    public LivreDto saveOrUpdate(LivreDto lvrDto) {
        ModelMapper m=new ModelMapper();
        Livre l=m.map(lvrDto, Livre.class);
        return m.map(livreRepository.saveAndFlush(l),LivreDto.class);
    }

}
