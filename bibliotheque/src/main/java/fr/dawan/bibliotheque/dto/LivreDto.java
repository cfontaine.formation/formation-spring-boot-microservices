package fr.dawan.bibliotheque.dto;

public class LivreDto {

    private long id;
    private String titre;
    private int anneeSortie;
    private String categorie;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getAnneeSortie() {
        return anneeSortie;
    }

    public void setAnneeSortie(int anneeSortie) {
        this.anneeSortie = anneeSortie;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "LivreDto [id=" + id + ", titre=" + titre + ", anneeSortie=" + anneeSortie + ", categorie=" + categorie
                + "]";
    }

}
