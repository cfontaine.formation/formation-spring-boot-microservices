package fr.dawan.bibliotheque;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import fr.dawan.bibliotheque.entities.Auteur;
import fr.dawan.bibliotheque.repositories.AuteurRepository;

@SpringBootTest
class TestAuteurRepository {

    @Autowired
    AuteurRepository auteurRepository;
    
    @Test
    void test() {
        System.out.println("________________________________________");
    
        PageRequest p=PageRequest.of(0, 10);
        List<Auteur> auteurs=auteurRepository.findAll(p).getContent();
        for(Auteur a : auteurs) {
            System.out.println(a);
        }
        
        auteurs=auteurRepository.findAlive();
        for(Auteur a : auteurs) {
            System.out.println(a);
        }
        

        
        auteurs=auteurRepository.findByLivreId(3);
        for(Auteur a : auteurs) {
            System.out.println(a);
        }
        p=PageRequest.of(0, 5);
        auteurs=auteurRepository.findTop5CountLivre(p);
        for(Auteur a : auteurs) {
            System.out.println(a);
        }
        System.out.println("________________________________________");
    }

}
