package fr.dawan.bibliotheque;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.dawan.bibliotheque.dto.LivreDto;
import fr.dawan.bibliotheque.service.LivreService;

@SpringBootTest
class TestLivreService {

    @Autowired
    private LivreService livreService; 
    
    @Test
    void test() {
       
       List<LivreDto> lstLvr=livreService.getAllLivre();
       for(LivreDto dto : lstLvr)
       {
           System.out.println(dto);
       }
       
       lstLvr=livreService.getAllLivre(0,10);
       for(LivreDto dto : lstLvr)
       {
           System.out.println(dto);
       }
       
       lstLvr=livreService.searchByTitre("dUn");
       for(LivreDto dto : lstLvr)
       {
           System.out.println(dto);
       }
        LivreDto dto=livreService.findById(2);
        System.out.println(dto);
        
        //livreService.deleteById(1);
        
    }

}
