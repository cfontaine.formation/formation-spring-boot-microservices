package fr.dawan.bibliotheque;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.dawan.bibliotheque.entities.Livre;
import fr.dawan.bibliotheque.repositories.LivreRepositrory;
@SpringBootTest
class TestLivreRepository {

    @Autowired 
    LivreRepositrory livreRepository;
    
    @Test
    void test() {
        
        List<Livre> livres=livreRepository.findByAnneeSortie(1962);
        for(Livre l : livres) {
            System.out.println(l);
        }
        
        livres=livreRepository.findByAnneeSortieBetween(1970, 1980);
        for(Livre l : livres) {
            System.out.println(l);
        }
        
        livres=livreRepository.findByTitreLikeIgnoreCase("%dUn%");
        for(Livre l : livres) {
            System.out.println(l);
        }
    }

}
