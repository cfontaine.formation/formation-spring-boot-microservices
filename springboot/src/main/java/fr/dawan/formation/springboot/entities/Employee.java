package fr.dawan.formation.springboot.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Version;
@Entity
public class Employee {
    
    @EmbeddedId
    private EmployeeId id;
    
    private String nom;

    @Version
    private int version; 
    
    @OneToMany(mappedBy = "employee")
    private List<Telephone> telephones=new ArrayList<>();
    
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name="emp_Rapport_reunion")
    private List<String>  rapportReunion;
    
    public EmployeeId getId() {
        return id;
    }

    public void setId(EmployeeId id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    

    public List<Telephone> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephone> telephones) {
        this.telephones = telephones;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", nom=" + nom + "]";
    }

}
