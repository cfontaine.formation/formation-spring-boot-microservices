package fr.dawan.formation.springboot.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import fr.dawan.formation.springboot.enums.Genre;

@Entity
@Table(name = "contacts")
//@TableGenerator(name="ContactGenrator")
public class Contact implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //generator = "ContactGenrator"
    private long id;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private Genre genre;
    
    @Column(name="prenom",length = 60)
    private String prenom;
    
    @Column(name="nom",length = 60)
    private String nom;
    
    @Transient
    private String nePasPersister;
    
    @Version
    private int version; 

    public Contact() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNePasPersister() {
        return nePasPersister;
    }

    public void setNePasPersister(String nePasPersister) {
        this.nePasPersister = nePasPersister;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Contact [id=" + id + ", prenom=" + prenom + ", nom=" + nom + "]";
    }
    
}
