package fr.dawan.formation.springboot.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "produit")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProduitDto {

    @XmlAttribute
    private long id;
    
    @XmlElement
    private String description;
    
    @XmlElement
    private double prix;

    
    private byte[] image;
    public ProduitDto() {
        super();
    }

    public ProduitDto(long id, String description, double prix) {
        super();
        this.id = id;
        this.description = description;
        this.prix = prix;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    
}
