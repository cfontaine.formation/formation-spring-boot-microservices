package fr.dawan.formation.springboot.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.dawan.formation.springboot.services.Userservice;

public class AppAuthProvider extends DaoAuthenticationProvider {

    @Autowired 
    PasswordEncoder passwordEncoder;
    
    @Autowired
    private Userservice userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // login / pasword
        UsernamePasswordAuthenticationToken tkAuth = (UsernamePasswordAuthenticationToken) authentication;
        String userPassword = tkAuth.getCredentials().toString();
        String userName = tkAuth.getName();
        UserDetails user = userService.loadUserByUsername(userName);
        if (user != null) {    // && userPassword.equals(user.getPassword())
            if (userPassword != null  &&  passwordEncoder.matches(userPassword, user.getPassword())){
                return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            }
            else {
                throw new BadCredentialsException(userName + ": Mauvais password" );
            }
        }else {
            throw new BadCredentialsException("Mauvais nom utilisateur" );
        }
        
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
