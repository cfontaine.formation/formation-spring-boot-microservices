package fr.dawan.formation.springboot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.dawan.formation.springboot.entities.User;
import fr.dawan.formation.springboot.repositories.UserRepository;

@Service
public class Userservice implements UserDetailsService {

    
    private UserRepository userRepository;
    
    @Autowired
    public Userservice(UserRepository userRepository) {
        this.userRepository=userRepository;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       User user=null;
       if(username!=null) {
           user=userRepository.findByUsername(username);
       }
       if(username==null) {
           throw new UsernameNotFoundException("User not found");
       }
        return user;
    }

}
