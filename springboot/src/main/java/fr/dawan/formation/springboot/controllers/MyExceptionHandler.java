package fr.dawan.formation.springboot.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.dawan.formation.springboot.utils.ApiError;



@ControllerAdvice
public class MyExceptionHandler extends ResponseEntityExceptionHandler {

    
    @ExceptionHandler(value= {Exception.class})
    ResponseEntity<Object> handleConflit(Exception ex,WebRequest request){
//     System.out.println("Handle global");
//        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT,request);
    
    ApiError errApi=new ApiError(HttpStatus.CONFLICT,ex.getMessage());
    return handleExceptionInternal(ex,errApi, new HttpHeaders(),HttpStatus.CONFLICT,request);
    }
}

