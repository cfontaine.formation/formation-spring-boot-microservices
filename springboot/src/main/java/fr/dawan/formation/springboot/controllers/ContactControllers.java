package fr.dawan.formation.springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.formation.springboot.entities.Contact;
import fr.dawan.formation.springboot.enums.Genre;
import fr.dawan.formation.springboot.repositories.ContactRepository;

@RestController
@RequestMapping("api/contacts")
public class ContactControllers {

    @Autowired
    ContactRepository contactDao;

    @GetMapping(produces = "application/json")
    public List<Contact> getContacts() {
        System.out.println(contactDao.countByName("Doe"));
        return contactDao.findByName("Doe");
    }
}
