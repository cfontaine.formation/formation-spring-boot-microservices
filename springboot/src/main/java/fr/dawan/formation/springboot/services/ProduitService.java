package fr.dawan.formation.springboot.services;

import java.util.List;

import fr.dawan.formation.springboot.dto.ProduitDto;

public interface ProduitService {

    
    List<ProduitDto> getAllProduits();
    
    List<ProduitDto> getAllProduits(int page, int nbElements);
    
    ProduitDto findbyId(long id);
    
    void deleteById(long id);
    
    ProduitDto saveOrUpdate(ProduitDto prDto);
}
