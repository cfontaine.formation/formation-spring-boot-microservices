package fr.dawan.formation.springboot.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import fr.dawan.formation.springboot.dto.ProduitDto;
import fr.dawan.formation.springboot.entities.Produit;
import fr.dawan.formation.springboot.mappers.ProduitMapper;
import fr.dawan.formation.springboot.repositories.ProduitRepository;

@Service
public class ProduitServiceImpl implements ProduitService {

    @Autowired
    private ProduitRepository produitRepository;

    @Autowired
    private ProduitMapper mapper;

    @Override
    public List<ProduitDto> getAllProduits() {
        // List<Produit> lp=produitRepository.findAll();
        // Conversion Manuel entity vers dto
        // List<ProduitDTO> res=new ArrayList<>();
//       for(Produit p: lp) {
//     
//           res.add( new ProduitDTO(p.getId(),p.getDescription(),p.getPrix(),p.getFournisseur().getNom()));
//       }
//       ModelMapper m =new ModelMapper();
//       for(Produit p: lp) {
//           res.add(m.map(p, ProduitDTO.class));
//       }

        return produitRepository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<ProduitDto> getAllProduits(int page, int nbElements) {
        
        return produitRepository.findAll(PageRequest.of(page, nbElements)).stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public ProduitDto findbyId(long id) {
        Optional<Produit> optProduit = produitRepository.findById(id);
        if (optProduit.isPresent()) {
            return mapper.toDto(optProduit.get());
        }
        return null;
    }

    @Override
    public void deleteById(long id) {
        produitRepository.deleteById(id);

    }

    @Override
    public ProduitDto saveOrUpdate(ProduitDto prDto) {
        Produit produit=mapper.fromDto(prDto);
        produit=produitRepository.saveAndFlush(produit);
        return mapper.toDto(produit);
    }

}
