package fr.dawan.formation.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import fr.dawan.formation.springboot.services.Userservice;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired 
    private Userservice userService; 
    
    @Autowired
    private AuthenticationProvider provider;
    
    
    
    @Bean
    public AuthenticationProvider getProvider() {
        AppAuthProvider provider=new AppAuthProvider();
        provider.setUserDetailsService(userService);
        return provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
        .disable()
        .authenticationProvider(provider)
        .authorizeRequests()
        .antMatchers(HttpMethod.POST,"/api/produits/upload/file").authenticated()
        .antMatchers(HttpMethod.POST,"/api/produits/upload/database").authenticated()
      //  .antMatchers("/api/produits").hasAnyAuthority("DELETE","SUPER")
        .antMatchers(HttpMethod.GET,"/api/produits").hasAnyAuthority("READ","SUPER")
        .antMatchers(HttpMethod.POST,"/api/produits").hasAnyAuthority("WRITE","SUPER")
        .antMatchers(HttpMethod.PUT,"/api/produits").hasAnyAuthority("WRITE","SUPER")
        .antMatchers(HttpMethod.DELETE,"/api/produits").hasAnyAuthority("DELETE","SUPER")
        .anyRequest().permitAll()
        .and()
        .httpBasic();
    }
    
    
    

}
