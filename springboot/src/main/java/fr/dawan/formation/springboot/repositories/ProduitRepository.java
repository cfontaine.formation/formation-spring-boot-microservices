package fr.dawan.formation.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.formation.springboot.entities.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

//    @Query(value = "SELECT p FROM Produit p JOIN FETCH p.fournisseurs f WHERE f.nom=:nameFournisseur ")
//    List<Produit> findByFournisseur(@Param("nameFournisseur") String nameFournisseur);

//    @Query(value = "SELECT p FROM Produit p JOIN FETCH p.fournisseurs f ")
//    List<Produit> findAll();
    
    
    @Procedure(value="GET_COUNT_BY_PRIX2")
   // @Query(value="CALL GET_COUNT_BY_PRIX(:montant)",nativeQuery = true)
    int countByPrixMax(@Param("montant") double montantMax); 
}
