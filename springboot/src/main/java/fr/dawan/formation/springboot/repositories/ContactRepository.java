package fr.dawan.formation.springboot.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.formation.springboot.entities.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

        List<Contact> findByNomAndPrenom(String n,String p);
        
        List<Contact> findByNomOrderByPrenom(String n);
        
        @Query(value="select c FROM Contact c WHERE c.nom = :search")
        List<Contact> findByName(@Param("search") String searchName);
        
        @Query(value="select COUNT(c) FROM Contact c WHERE c.nom = :search")
        int countByName(@Param("search") String searchName);
        
        List<Contact> findByPrenom(String prenom,Sort sort);
        List<Contact> findByPrenom(String prenom,Pageable pageable);
        
        List<Contact> findTop10ByPrenom(String prenom);
;}
