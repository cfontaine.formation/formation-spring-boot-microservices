package fr.dawan.formation.springboot.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.dawan.formation.springboot.entities.Contact;

@Repository
public class CustomRepositoryImpl implements CustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Contact> findBy(String prenom, String nom) {
        Map<String, Object> params = new HashMap<>();
        String s = "SELECT c FROM Contact c";
        if (prenom != null) {
            s += " WHERE c.prenom = :prenom ";
            params.put("prenom", prenom);
        }
        if (nom != null) {
            if (!s.contains("WHERE")) {
                s += " WHERE ";
            } else {
                s += " AND";
            }
            s += " c.nom=:nom";
            params.put("nom", nom);
        }
        TypedQuery<Contact> q = em.createQuery(s, Contact.class);
        for (Entry<String, Object> entry : params.entrySet()) {
            q.setParameter(entry.getKey(), entry.getValue());
        }

        List<Contact> lstContact = q.getResultList();
        return lstContact;

    }

}
