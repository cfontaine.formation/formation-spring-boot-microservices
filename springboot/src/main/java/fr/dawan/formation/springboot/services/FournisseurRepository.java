package fr.dawan.formation.springboot.services;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.formation.springboot.entities.Fournisseur;

public interface FournisseurRepository extends JpaRepository<Fournisseur, Long> {
    List<Fournisseur>findByNom(String nom);
}
