package fr.dawan.formation.springboot.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.formation.springboot.dto.ProduitDto;
import fr.dawan.formation.springboot.services.ProduitService;

@RestController
@RequestMapping("api/produits")
public class ProduitController {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    ProduitService produitService;

    @GetMapping(produces = "application/json")
    public List<ProduitDto> getAll() {
        return produitService.getAllProduits();
    }

    @GetMapping(value = "/{id}", produces = { "application/json", "application/xml" })
    public ProduitDto getById(@PathVariable long id) {
        return produitService.findbyId(id);
    }

    @GetMapping(params = { "page", "size" }, produces = "application/json")
    public List<ProduitDto> getAllPAge(@RequestParam(required = true) int page,
            @RequestParam(required = true) int size) {
        return produitService.getAllProduits(page, size);
    }
    
   // @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @DeleteMapping(value = "/{id}", produces = "text/plain")
    public ResponseEntity<?> remove(@PathVariable long id) {
        try {
            produitService.deleteById(id);
        //    return "Suppression OK";
            return new ResponseEntity<String>(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            e.printStackTrace();
        //    return "Erreur" + e.getMessage();
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);  
        }
    }
    
    @PostMapping(consumes="application/json", produces="application/json")
    public ProduitDto save(@RequestBody ProduitDto prDto) {
        return produitService.saveOrUpdate(prDto);
    }
    
    @PutMapping(consumes="application/json", produces="application/json")
    public ProduitDto update(@RequestBody ProduitDto prDto) {
        return produitService.saveOrUpdate(prDto);
    }
    
    @PostMapping(value="upload/file",consumes="multipart/form-data")
    String upload(@RequestParam(value="fileUpload", required =true) MultipartFile file) throws IOException {
        
        byte[] data=file.getBytes();
        Path pathFile= Paths.get("C:\\Formations\\TestIO\\",file.getOriginalFilename());
        Files.write(pathFile, data);
        return "file uploaded" + file.getBytes().length;
    }
    
    @PostMapping(value="upload/database",consumes="multipart/form-data")
    ProduitDto uploadDatabase(@RequestParam(value="fileUpload", required =true) MultipartFile file) throws IOException {
        
        ProduitDto produitDto=new ProduitDto();
        produitDto.setDescription("test");
        produitDto.setPrix(23.0);
        produitDto.setImage(file.getBytes());
        return produitService.saveOrUpdate(produitDto);
    }
    
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler({IOException.class})
    public String handleException(IOException ex)
    {
        System.out.println("Handle lobal");
        return "IOException"+ ex.getMessage();
    }
    
    @GetMapping("exception/io")
    public void testIOException() throws IOException
    {
        
        throw new IOException("=>IO Exception");
    }

    @GetMapping("exception")
    public void testException() throws Exception
    {
        throw new Exception("=> Exception");
    }


}