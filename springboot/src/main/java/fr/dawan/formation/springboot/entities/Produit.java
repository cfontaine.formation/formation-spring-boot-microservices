package fr.dawan.formation.springboot.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;

@Entity
public class Produit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String description;
    
    private double prix;
    
    
    
    
    @ManyToMany(mappedBy = "produits",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
   // @JoinTable(name="produit2Founisseur", joinColumns = @JoinColumn(name="id_produit"), inverseJoinColumns =@JoinColumn(name="id_fournisseur") )
    private List<Fournisseur> fournisseurs=new ArrayList<>();
//    @ManyToOne
//    private Fournisseur fournisseur;
    
    @Lob
    private byte[] image;
    
    
    
    
    public Produit() {
        super();
    }


    public Produit(String description, double prix) {
    super();
    this.description = description;
    this.prix = prix;
}


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public double getPrix() {
        return prix;
    }


    public void setPrix(double prix) {
        this.prix = prix;
    }


    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }


    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    

//    public Fournisseur getFournisseur() {
//        return fournisseur;
//    }
//
//
//    public void setFournisseur(Fournisseur fournisseur) {
//        this.fournisseur = fournisseur;
//    }


    
    
    public byte[] getImage() {
        return image;
    }


    public void setImage(byte[] image) {
        this.image = image;
    }


    @Override
    public String toString() {
        return "Produit [id=" + id + ", description=" + description + ", prix=" + prix  +"]";
    } 
    
    
}
