package fr.dawan.formation.springboot.entities;

import javax.persistence.Entity;

@Entity
public class Category extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    
    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Category [nom=" + nom + ", toString()=" + super.toString() + "]";
    }
    
    

}
