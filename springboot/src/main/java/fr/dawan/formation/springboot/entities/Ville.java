package fr.dawan.formation.springboot.entities;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Ville implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    private long id; 
    
    private String nomVille;
    
//    @OneToOne
//    @JoinColumn(name="id_maire",referencedColumnName = "id")
    @Embedded
    Maire maire;
    
    public Maire getMaire() {
        return maire;
    }

    public void setMaire(Maire maire) {
        this.maire = maire;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nomVille;
    }

    public void setNom(String nom) {
        this.nomVille = nom;
    }

    @Override
    public String toString() {
        return "Ville [id=" + id + ", nom=" + nomVille + "]";
    }
    
    
}
