package fr.dawan.formation.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
//	     SpringApplication app = new SpringApplication(SpringbootApplication.class);
//      app.setBanner((Environment environment, Class<?> sourceClass, PrintStream out)-> 
//            out.println("====== Dawan ======");
//            });
//      app.setBannerMode(Mode.LOG);
//      app.run(args);
	    
		SpringApplication.run(SpringbootApplication.class, args);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}
}
