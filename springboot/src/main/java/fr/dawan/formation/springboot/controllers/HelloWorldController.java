package fr.dawan.formation.springboot.controllers;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class HelloWorldController {
    
    private static final Logger LOGGER=LoggerFactory.getLogger(HelloWorldController.class);

//  @Value("${cle}")
//  private String testValue;
  
  @RequestMapping("/")
  public String helloWorld() {
      return "Hello World!!!"; /* + testValue;*/
  }
  
  @RequestMapping("/log")
  public String testLog() {
      
      LOGGER.warn("Warning Message");
      LOGGER.error("Error Message");
      return "Test Log";
  }
}
