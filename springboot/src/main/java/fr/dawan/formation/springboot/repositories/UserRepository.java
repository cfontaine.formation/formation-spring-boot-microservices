package fr.dawan.formation.springboot.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.formation.springboot.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    User findByUsername(String userName);

}
