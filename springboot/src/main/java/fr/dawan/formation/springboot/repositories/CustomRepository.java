package fr.dawan.formation.springboot.repositories;

import java.util.List;

import fr.dawan.formation.springboot.entities.Contact;

public interface CustomRepository {

  List<Contact> findBy(String prenom,String nom);  
}
