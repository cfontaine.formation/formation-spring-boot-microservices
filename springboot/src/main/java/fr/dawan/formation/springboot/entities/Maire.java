package fr.dawan.formation.springboot.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;

//@Entity
@Embeddable
public class Maire implements Serializable{

    private static final long serialVersionUID = 1L;

//    @Id
//    private long id;
    
    private String nom;

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Maire [nom=" + nom + "]";
    }
    
    
}
