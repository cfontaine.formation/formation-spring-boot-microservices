package fr.dawan.formation.springboot.entities;

import javax.persistence.Entity;

@Entity
//@DiscriminatorValue(value = "Epargne")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;
    
    private double taux=0.5;


    public double getTaux() {
        return taux;
    }


    public void setTaux(double taux) {
        this.taux = taux;
    }


    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }


     
}
