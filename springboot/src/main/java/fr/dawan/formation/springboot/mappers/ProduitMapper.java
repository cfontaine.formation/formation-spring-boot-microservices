package fr.dawan.formation.springboot.mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import fr.dawan.formation.springboot.dto.ProduitDto;
import fr.dawan.formation.springboot.entities.Fournisseur;
import fr.dawan.formation.springboot.entities.Produit;

@Component
public class ProduitMapper {

    private ModelMapper mapper;
    
//    private Converter<String,String> toUpperCase= new AbstractConverter<String, String>() {
//
//        @Override
//        protected String convert(String source) {
//            if(source!=null) {
//                return source.toUpperCase();
//            }
//            return null;
//        }
//    };

//// private Converter<String,String> toUpperCase= cx ->cx.getSource()!=null? cx.getSource().toUpperCase():null;
//    
////   private Converter<List<Fournisseur>,String> convFournisseurToString= cx ->cx.getSource()!=null?cx.getSource().stream().map(c->new StringBuffer(c.getNom())).collect(Collectors.joining(" , ")):null;
////   
////   private Converter<String,List<Fournisseur>> convStringToFournisseur= cx ->{if(cx.getSource()!=null) {
////        List<Fournisseur> convList=new ArrayList<>();
////        List<String> lst=Stream.of(cx.getSource().split(",", -1)).collect(Collectors.toList());
////        for(String s : lst) {
////            Fournisseur f=new Fournisseur();
////            f.setNom(s);
////            convList.add(f);
////        }
////        return  convList;
////    }
////    return null;
////    };
////    
    public ProduitMapper() {
        mapper=new ModelMapper();
    }
    
    public ProduitDto toDto(Produit p) {
        mapper.typeMap(Produit.class,ProduitDto.class).addMappings(mapper ->{
            mapper.map(src->src.getId(),ProduitDto::setId);
            mapper.map(src->src.getDescription(),ProduitDto::setDescription);
          //mapper.using(toUpperCase).map(src->src.getDescription(), ProduitDto::setDescription);
            mapper.map(src->src.getPrix(),ProduitDto::setPrix);
            mapper.map(src->src.getImage(),ProduitDto::setImage);
    //        mapper.using(convFournisseurToString).map(src->src.getFournisseurs(), ProduitDto::setFournisseur);
           // mapper.map(src->src.getFournisseur().getNom(),ProduitDto::setFournisseur); // MANY TO ONE
        });
        return mapper.map(p, ProduitDto.class);
    }
    
    public Produit fromDto(ProduitDto prdto) {
        mapper.typeMap(ProduitDto.class,Produit.class).addMappings(mapper ->{
            mapper.map(src->src.getId(),Produit::setId);
            mapper.map(src->src.getDescription(),Produit::setDescription);
            mapper.map(src->src.getPrix(),Produit::setPrix);
            mapper.map(src->src.getImage(),Produit::setImage);
      //      mapper.using(convStringToFournisseur).map(src->src.getFournisseur(), (dest,v)->dest.getFournisseurs());
          //  mapper.map(src->src.getFournisseur(),(dest,v)->dest.getFournisseur());// MANY TO ONE
        });
        return mapper.map(prdto, Produit.class);
    }
}
