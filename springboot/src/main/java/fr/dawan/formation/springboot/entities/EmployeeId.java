package fr.dawan.formation.springboot.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class EmployeeId implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private long numero;
    private long groupe;

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public long getGroupe() {
        return groupe;
    }

    public void setGroupe(long groupe) {
        this.groupe = groupe;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (groupe ^ (groupe >>> 32));
        result = prime * result + (int) (numero ^ (numero >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EmployeeId other = (EmployeeId) obj;
        if (groupe != other.groupe)
            return false;
        if (numero != other.numero)
            return false;
        return true;
    }

}
