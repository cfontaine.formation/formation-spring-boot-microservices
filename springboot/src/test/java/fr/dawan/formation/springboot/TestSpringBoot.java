package fr.dawan.formation.springboot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.formation.springboot.controllers.ProduitController;
import fr.dawan.formation.springboot.dto.ProduitDto;
import fr.dawan.formation.springboot.entities.Produit;
import fr.dawan.formation.springboot.repositories.ProduitRepository;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@Transactional
class TestSpringBoot {
    @Autowired
    private ProduitController produitController;
    
    @Autowired
    private ProduitRepository repository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void test() {
        assertThat(produitController).isNotNull();
    }

    @Test
    @DisplayName("Test de /api/produit et GET findALL ")
    void testFindAll() {
        try {
            mockMvc.perform(get("/api/produits")).andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].description", is("Ecran"))).andExpect(jsonPath("$[0].prix", is(42.0)));
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    void testFindById() {
        try {
            String json = mockMvc.perform(get("/api/produits/1")).andReturn().getResponse().getContentAsString();
            objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
            ProduitDto resTest = objectMapper.readValue(json, ProduitDto.class);
            assertEquals("Ecran", resTest.getDescription());
            assertEquals(42.0, resTest.getPrix());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }
    
    @Test
    void testSave() {
        ProduitDto prd=new ProduitDto(0L,"new",12.6);
        objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        try {
            String json=objectMapper.writeValueAsString(prd);
            
            String jsonR=mockMvc.perform(post("/api/produits")
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON)
                                            .content(json)
                                             )
                    .andReturn().getResponse().getContentAsString();
            ProduitDto resTest = objectMapper.readValue(jsonR, ProduitDto.class);
            assertNotEquals(0L, resTest.getId());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        } 
    }
    
    @Test
    void testProduitRepositorySave() {
        Produit prod=new Produit("TEST2", 10.0);
        Produit res= repository.saveAndFlush(prod);
        assertNotEquals(0L, res.getId());
    }
    

}
