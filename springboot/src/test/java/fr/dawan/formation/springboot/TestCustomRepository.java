package fr.dawan.formation.springboot;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.dawan.formation.springboot.entities.Contact;
import fr.dawan.formation.springboot.repositories.CustomRepository;

@SpringBootTest
class TestCustomRepository {

    @Autowired
    private CustomRepository customRepository;
    
    @Test
    void test() {
        List<Contact> lstC=customRepository.findBy(null, "Doe");
        for(Contact c : lstC) {
            System.out.println(c);
        }
        
        lstC=customRepository.findBy("John", "Doe");
        for(Contact c : lstC) {
            System.out.println(c);
        }
    }

}
