package fr.dawan.formation.springboot;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import fr.dawan.formation.springboot.entities.Contact;
import fr.dawan.formation.springboot.enums.Genre;
import fr.dawan.formation.springboot.repositories.ContactRepository;

@SpringBootTest
class TestRepository {

    @Autowired
    ContactRepository contactRepository;
    
    @Test
    void test() {
        Contact c=null;
        // ajouter un contact
        c=new Contact();
        c.setGenre(Genre.HOMME);
        c.setPrenom("Jo");
        c.setNom("Dalton ");
        contactRepository.save(c);

        
        System.out.println(contactRepository.count());
        
        System.out.println(contactRepository.findById(1L));
        
        List<Contact> contacts=contactRepository.findAll();
        for(Contact co : contacts) {
            System.out.println(co);
        }
        
        contactRepository.delete(c);
        contacts=contactRepository.findAll();
        for(Contact co : contacts) {
            System.out.println(co);
        }
        
        System.out.println(contactRepository.countByName("Doe"));
        contacts=contactRepository.findByName("Doe");
        for(Contact co : contacts) {
            System.out.println(co);
        }
        
        contacts=contactRepository.findByNomAndPrenom("Doe", "Jane");
        for(Contact co : contacts) {
            System.out.println(co);
        }
        
        
        
        contacts=contactRepository.findByNomOrderByPrenom("Doe");
        for(Contact co : contacts) {
            System.out.println(co);
        }
        
        
        Sort sort= Sort.by("nom").descending();
        contacts=contactRepository.findByPrenom("Jo", sort);
        for(Contact co : contacts) {
            System.out.println(co);
        }
        
        Pageable first=PageRequest.of(2, 20,sort);
        contacts=contactRepository.findByPrenom("Jo", Pageable.unpaged());//first
        for(Contact co : contacts) {
            System.out.println(co);
        }
        
        contacts=contactRepository.findTop10ByPrenom("Jo");
        for(Contact co : contacts) {
            System.out.println(co);
        }
    }

}
