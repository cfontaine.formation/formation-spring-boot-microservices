package fr.dawan.formation.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.dawan.formation.springboot.repositories.ProduitRepository;

@SpringBootTest
class TestRepositoryProduit {

    @Autowired
    ProduitRepository produitRepository;
    
    @Test
    void test() {
      System.out.println( produitRepository.countByPrixMax(20.0));
    }

}
