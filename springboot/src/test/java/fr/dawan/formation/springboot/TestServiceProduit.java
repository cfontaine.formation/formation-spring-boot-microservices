package fr.dawan.formation.springboot;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.dawan.formation.springboot.dto.ProduitDto;
import fr.dawan.formation.springboot.services.ProduitService;
@SpringBootTest
class TestServiceProduit {

    @Autowired
   private ProduitService produitService;
    
    @Test
    void test() {
//        ProduitDto prDto=new ProduitDto(0,"Cable RJ45",12.5,"Fournisseur A");
//        prDto=produitService.saveOrUpdate(prDto);
//        System.out.println(prDto);
        
        
        
        List<ProduitDto> lstProduitDto=produitService.getAllProduits(0,2);
        for(ProduitDto prod : lstProduitDto)
        {
            System.out.println(prod);
        }
        
        ProduitDto prDto=produitService.findbyId(3);
        System.out.println(prDto);
        
        
    }

}
