package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.CategoryDao;
import fr.dawan.springcore.beans.ServiceCategory;

// Configuration en XML
public class MainXml {

    public static void main(String[] args) {
        // Création du conteneur spring
        // Les méta-données sont contenus dans le fichier beans.xml
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        // Récupération des beans
        CategoryDao dao = context.getBean("daoC", CategoryDao.class);
        System.out.println(dao);

//        CategoryDao dao2=context.getBean("daoC2", CategoryDao.class);
//        System.out.println(dao2);

//        ServiceCategory serviceCe=context.getBean("serviceCe", ServiceCategory.class);
//        System.out.println(serviceCe);

        ServiceCategory serviceCi = context.getBean("serviceC", ServiceCategory.class);
        System.out.println(serviceCi);

        // Destruction du conteneur spring
        ((AbstractApplicationContext) context).close();
    }

}
