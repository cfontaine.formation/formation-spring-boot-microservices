package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.springcore.beans.CategoryDao;
import fr.dawan.springcore.beans.ServiceCategory;

@Configuration // indique que la classe fourni des définitions de bean
@ComponentScan(basePackages = "fr.dawan.springcore") // On indique les packages où rechercher les composants
public class AppConfig {

    @Bean(name = "daoC") // @Bean indique que la méthode instancie, configure et initialise un bean géré
                         // par le conteneur ioc Spring
    public CategoryDao daoC() {
        return new CategoryDao("data");
    }

    @Bean(name = "serviceC")
    // La dépendances peut-être matérialiser à l'aide des paramètres de la méthode
    public ServiceCategory serviceC(CategoryDao daoC) {
        return new ServiceCategory(daoC);
    }
}
