package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.CategoryDao;
import fr.dawan.springcore.beans.ServiceCategory;

// Configuration en XML avec les annotations
public class MainXmlAnnotation {

    public static void main(String[] args) {
        // Création du conteneur spring
        // Les méta-données sont contenus dans le fichier beans.xml
        ApplicationContext context = new ClassPathXmlApplicationContext("beans_annotation.xml");

        // Récupération des beans
        CategoryDao dao = context.getBean("daoC", CategoryDao.class);
        System.out.println(dao);

        ServiceCategory service = context.getBean("serviceC", ServiceCategory.class);
        System.out.println(service);

        // Destruction du conteneur spring
        ((AbstractApplicationContext) context).close();
    }

}
