package fr.dawan.springcore.beans;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

public class ServiceCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Autowired // Si la configuration par annotation est activé
               // permet de faire de l'injection automatique de dépendances basée sur le type
               // Autowired peut se placer sur l'attribut
    private CategoryDao daoCategory;

    public ServiceCategory() {
        super();
    }

    // @Autowired // Autowired peut se placer sur le constructeur
    public ServiceCategory(CategoryDao daoCategory) {
        super();
        this.daoCategory = daoCategory;
    }

    public CategoryDao getDaoCategory() {
        return daoCategory;
    }

    // @Autowired // Autowired peut se placer sur le setter
    public void setDaoCategory(CategoryDao daoCategory) {
        this.daoCategory = daoCategory;
    }

    @Override
    public String toString() {
        return "ServiceCategory [daoCategory=" + daoCategory + "]";
    }

}
