package fr.dawan.springcore.beans;

import java.io.Serializable;

public class CategoryDao implements Serializable {

    private static final long serialVersionUID = 1L;

    private String data;
    
    public CategoryDao() {
        super();
    }

    public CategoryDao(String data) {
        super();
        System.out.println("Constructeur");
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        System.out.println("setData");
        this.data = data;
    }

    @Override
    public String toString() {
        return "CategoryDao [data=" + data + "]";
    }

}
