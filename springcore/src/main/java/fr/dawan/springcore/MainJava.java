package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.CategoryDao;
import fr.dawan.springcore.beans.ServiceCategory;

// Configuration en Java
public class MainJava {

    public static void main(String[] args) {

        // Création du conteneur spring
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        // Récupération des beans
        CategoryDao dao = context.getBean("daoC", CategoryDao.class);
        System.out.println(dao);

        ServiceCategory service = context.getBean("serviceC", ServiceCategory.class);
        System.out.println(service);

        // Destruction du conteneur spring
        ((AbstractApplicationContext) context).close();
    }

}
